<?php

/**
 * @file
 * This file contains the admin form for the module.
 */

/**
 * {@inheritdoc}
 */
function conditional_redirect_configuration_form() {

  // Initialize the form.
  $form = array();

  // The login path to redirect to.
  $form['conditional_redirect_login_path'] = array(
    '#title' => t('Login path'),
    '#description' => t('The path to the login page to which the user will be redirected.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('conditional_redirect_login_path', 'user/login'),
  );

  // The content type settings fieldset.
  $form['content_types'] = array(
    '#title' => t('Content type settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Initialize the options.
  $options = array();

  // Get the content types.
  foreach (node_type_get_types() as $info) {
    $options[$info->type] = $info->name;
  }

  // The checkboxes for the login exclude content types.
  $form['content_types']['conditional_redirect_exclude_content_types'] = array(
    '#title' => t('Exclude content types'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#description' => t('Content types to exclude from redirecting to the login page.'),
    '#default_value' => variable_get('conditional_redirect_exclude_content_types', array()),
  );

  // The path settings fieldset.
  $form['paths'] = array(
    '#title' => t('Path settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // The textarea for the login exclude paths.
  $form['paths']['conditional_redirect_exclude_paths'] = array(
    '#title' => t('Exclude paths'),
    '#type' => 'textarea',
    '#description' => t('Links to exclude from redirecting to the login page. One per line.'),
    '#default_value' => variable_get('conditional_redirect_exclude_paths', ''),
  );

  // The menu link settings fieldset.
  $form['menu_link'] = array(
    '#title' => t('Menu link settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Include the menu functions.
  module_load_include('inc', 'conditional_redirect', 'includes/menu');

  // Get the menus.
  $menu_list = _conditional_redirect_get_menus();

  // Loop through the menu list to create fieldsets.
  foreach ($menu_list as $menu_name => $menu_title) {

    // Get the menu links in the menu.
    $links = _conditional_redirect_get_menu_links($menu_name);

    // Check if the menu has links.
    if (!empty($links)) {

      // Create a fieldset per menu.
      $form['menu_link'][$menu_name] = array(
        '#title' => $menu_title,
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      // Create the checkboxes per menu.
      $form['menu_link'][$menu_name]['conditional_redirect_settings_' . $menu_name] = array(
        '#type' => 'checkboxes',
        '#title' => $menu_title,
        '#default_value' => variable_get('conditional_redirect_settings_' . $menu_name, array()),
        '#options' => $links,
        '#description' => t('Menu links to exclude from redirecting to the login page.'),
      );
    }
  }

  // Return the form with saved settings.
  return system_settings_form($form);
}
