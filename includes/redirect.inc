<?php

/**
 * @file
 * This file holds the redirect related functions for the module.
 */

/**
 * Perform the redirect.
 */
function _condition_redirect_perform_redirect() {

  // Include the redirect path functions.
  module_load_include('inc', 'conditional_redirect', 'includes/redirect_path');

  // Request the current path.
  $destination = request_path();

  // Get the redirect path.
  $redirect_path = _conditional_redirect_get_redirect_path();

  // Initialize the array.
  $options = array();

  // Check the destination.
  if (isset($_GET['destination'])) {

    // Save the destination.
    $destination = $_GET['destination'];

    // Unset the destination.
    unset($_GET['destination']);
  }

  // Check the destination and make sure to not add the logout as destination.
  if (!empty($destination) && $destination != 'user/logout' && $destination != 'user/login') {

    // Set the destination.
    $options['query'] = array(
      'destination' => filter_xss($destination),
    );
  }

  // Perform the redirect.
  drupal_goto($redirect_path, $options);
}

/**
 * Check if we need to redirect.
 */
function _conditional_redirect_needs_to_redirect() {

  // Include the redirect path functions.
  module_load_include('inc', 'conditional_redirect', 'includes/redirect_path');
  module_load_include('inc', 'conditional_redirect', 'includes/redirect_type');
  module_load_include('inc', 'conditional_redirect', 'includes/redirect_menu');

  // Check if the path is excluded.
  $path_excluded = _conditional_redirect_path_is_excluded();
  $type_excluded = _conditional_redirect_type_is_excluded();
  $menu_excluded = _conditional_redirect_menu_is_excluded();

  // Return TRUE if the user is not logged in and none are excluded.
  return !user_is_logged_in() && !$path_excluded && !$type_excluded && !$menu_excluded;
}
