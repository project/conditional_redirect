<?php

/**
 * @file
 * This file holds the redirect path related functions for the module.
 */

/**
 * Get the direct path.
 *
 * @return string
 *   Returns the redirect path as string.
 */
function _conditional_redirect_get_redirect_path() {

  // Get the settings for the redirect path.
  $path = variable_get('conditional_redirect_login_path', 'user/login');

  // Allow other users to alter the path.
  drupal_alter('conditional_redirect_redirect_path', $path);

  // Return the redirect path array.
  return $path;
}

/**
 * Get the array of paths excluded by default.
 *
 * @return array
 *   Returns the array of paths excluded by default.
 */
function _conditional_redirect_get_default_excluded_paths() {

  // Set the array of paths.
  $paths = array(
    'user',
    'user/login',
  );

  // Allow other modules to override the array of paths excluded by default.
  drupal_alter('conditional_redirect_paths_excluded_by_default', $paths);

  // Return the paths.
  return $paths;
}

/**
 * Get the array of paths excluded by settings.
 *
 * @return array
 *   Returns the array of paths excluded by settings.
 */
function _conditional_redirect_get_settings_excluded_paths() {

  // Initialize the paths.
  $paths = array();

  // Get the exclude paths variable.
  $variable = variable_get('conditional_redirect_exclude_paths', '');

  // Check if there are paths configured.
  if (!empty($variable)) {

    // Explode the exclude paths variable by a new line.
    $paths = explode(PHP_EOL, $variable);
  }

  // Return the array of paths.
  return $paths;
}

/**
 * Check if the path is excluded by default.
 *
 * @return bool
 *   Returns TRUE if the path is in the array of the default excluded paths.
 */
function _conditional_redirect_path_excluded_by_default() {

  // Get the current path.
  $path = request_path();

  // Get the array of paths excluded by default.
  $default_paths = _conditional_redirect_get_default_excluded_paths();

  // Explode the path on a slash.
  $parts = explode('/', $path);

  // Check the parts.
  if (!empty($parts)) {

    // Initialize the string.
    $string = '';

    // Check all parts.
    foreach ($parts as $part) {

      // Check if the string with star is in the array.
      if (in_array($string . '*', $default_paths)) {
        return TRUE;

        break;
      }
      // Otherwise add the part to the string.
      else {
        $string .= $part . '/';
      }
    }
  }

  // Return TRUE if the path is in the array of the default excluded paths.
  return in_array($path, $default_paths);
}

/**
 * Check if the path is excluded by the settings.
 *
 * @return bool
 *   Returns TRUE if the path is in the array of the settings excluded paths.
 */
function _conditional_redirect_path_excluded_by_setting() {

  // Get the global base url.
  global $base_url;

  // Get the request path.
  $request_path = request_path();
  $absolute_request_path = $base_url . '/' . $request_path;

  // Get the current path.
  $current_path = current_path();
  $absolute_current_path = $base_url . '/' . $current_path;

  // Get the array of paths excluded by settings.
  $default_paths = _conditional_redirect_get_settings_excluded_paths();

  // Return TRUE if the path is in the array of the default excluded paths.
  return (
    in_array($request_path, $default_paths) ||
    in_array($absolute_request_path, $default_paths) ||
    in_array($current_path, $default_paths) ||
    in_array($absolute_current_path, $default_paths)
  );
}

/**
 * Check if the path is excluded from redirecting.
 *
 * @return bool
 *   Returns TRUE if the path is neither excluded by default nor by setting.
 */
function _conditional_redirect_path_is_excluded() {

  // Check if the path is excluded by default.
  $by_default = _conditional_redirect_path_excluded_by_default();

  // Check if the path is excluded by setting.
  $by_setting = _conditional_redirect_path_excluded_by_setting();

  // Return TRUE if either are excluded.
  return $by_default || $by_setting;
}
