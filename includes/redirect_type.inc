<?php

/**
 * @file
 * This file holds the redirect type related functions for the module.
 */

/**
 * Check if the content type is excluded from the redirect.
 */
function _conditional_redirect_type_is_excluded() {

  // Include the node functions.
  module_load_include('inc', 'conditional_redirect', 'includes/node');

  // Get the node type of the current page.
  $type = _conditional_redirect_get_current_node_type();

  // Get the excluded content types.
  $excluded_types = _conditional_redirect_get_excluded_content_types();

  // Check if the type is in the array of excluded content types.
  return in_array($type, $excluded_types);
}

/**
 * Get the excluded content types.
 */
function _conditional_redirect_get_excluded_content_types() {

  // Initialize the paths.
  $content_types = array();

  // Get the excluded content types variable.
  $variable = variable_get('conditional_redirect_exclude_content_types', '');

  // Check if there are paths configured.
  if (!empty($variable)) {

    // Loop through the variable.
    foreach ($variable as $type => $status) {

      // Check for status 1.
      if ($status) {

        // Add the type to the array.
        $content_types[] = $type;
      }
    }
  }

  // Return the array of types.
  return $content_types;
}
