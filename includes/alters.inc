<?php

/**
 * @file
 * This file holds the alter related functions for the module.
 */

/**
 * Implements hook_menu_link_alter().
 */
function conditional_redirect_menu_link_alter(&$item) {

  // Only hide links when the user is not logged in.
  if (!user_is_logged_in()) {
    $item['options']['alter'] = TRUE;
  }
}

/**
 * Implements hook_translated_menu_link_alter().
 */
function conditional_redirect_translated_menu_link_alter(&$item, $map) {

  // Don't alter hidden links or links that you don't have access to.
  if (!$item['hidden']) {

    // Get the menu name.
    $menu_name = $item['menu_name'];

    // Get the menu link id.
    $mlid = $item['mlid'];

    // Get the variable.
    $variable = variable_get('conditional_redirect_settings_' . $menu_name, array());

    // Check if the menu link id is in the array of settings.
    if (!in_array($mlid, $variable)) {

      // Disable access.
      $item['access'] = FALSE;
    }
  }
}
