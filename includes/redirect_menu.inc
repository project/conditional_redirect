<?php

/**
 * @file
 * This file holds the redirect menu related functions for the module.
 */

/**
 * Check if the menu (link) is excluded.
 *
 * Todo - This can be done in a much faster way.
 *
 * @return bool
 *   Returns TRUE if the menu (link) is excluded.
 */
function _conditional_redirect_menu_is_excluded() {

  // Include the menu functions.
  module_load_include('inc', 'conditional_redirect', 'includes/menu');

  // Get the menu link ids for the current path.
  $menu_link_ids = _conditional_redirect_get_menu_links_for_current_path();

  // Check if there are menu links.
  if (!empty($menu_link_ids)) {

    // Get the list of menus.
    $menu_list = _conditional_redirect_get_menus();

    // Loop through the menu link ids.
    foreach ($menu_link_ids as $mlid) {

      // Loop through the list of menus.
      foreach ($menu_list as $menu_name => $menu_title) {

        // Get the settings for the menu.
        $variable = variable_get('conditional_redirect_settings_' . $menu_name, array());

        // Return TRUE if the menu link id is in the array.
        if (in_array($mlid, $variable, TRUE)) {
          return TRUE;
        }
      }
    }
  }

  // Return FALSE by default.
  return FALSE;
}
