<?php

/**
 * @file
 * This file holds the node related functions for the module.
 */

/**
 * Get the node type of the current page.
 *
 * @return string|bool
 *   Returns the node type as string if found or FALSE.
 */
function _conditional_redirect_get_current_node_type() {

  // Get the menu item.
  $item = menu_get_item();

  // Check for the node path.
  if (!empty($item['path']) && $item['path'] == 'node/%') {

    // Check the map for the node id.
    if (!empty($item['map'][0]) && $item['map'][0] == 'node' && !empty($item['map'][1]) && is_object($item['map'][1])) {

      // Get the node.
      $node = $item['map'][1];

      // Check the node.
      if (!empty($node)) {

        // Return the node type.
        return $node->type;
      }
    }
  }

  // Return FALSE by default.
  return FALSE;
}
