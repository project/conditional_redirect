<?php

/**
 * @file
 * This file holds the menu related functions for the module.
 */

/**
 * Get the menus.
 *
 * @return array
 *   Returns the menus as array.
 */
function _conditional_redirect_get_menus() {

  // Get the menu names.
  $menu_list = menu_get_menus();

  // Allow alteration of the list of modules to include in the settings.
  drupal_alter('conditional_redirect_menu_list', $menu_list);

  // Return the menu list.
  return $menu_list;
}

/**
 * Get the menu links of a menu.
 *
 * @param string $menu_name
 *   The name of the menu.
 *
 * @return array
 *   Array {@inheritdoc}.
 */
function _conditional_redirect_get_menu_links($menu_name) {

  // Initialize the menu items array.
  $menu_items = array();

  // Get the tree data.
  $tree = menu_tree_all_data($menu_name);

  // Check the data.
  if (!empty($tree)) {

    // Build the menu links array.
    _conditional_redirect_build_menu_links_array($tree, $menu_items);
  }

  // Allow other modules to alter the menu links.
  drupal_alter('conditional_redirect_menu_links', $menu_items);

  // Return the menu items array.
  return $menu_items;
}

/**
 * Build the menu links array from a tree.
 *
 * @param mixed $tree
 *   The tree to build from.
 * @param array $menu_items
 *   The menu items array to create.
 */
function _conditional_redirect_build_menu_links_array(&$tree, array &$menu_items) {

  // Check the tree.
  if (!empty($tree['tree'])) {
    $tree = $tree['tree'];
  }

  // Loop through the tree.
  foreach ($tree as $key => $item) {

    // Check if the title is not empty.
    if (!empty($item['link']['link_title']) && !$item['link']['hidden']) {

      // Get the menu link id.
      $mlid = $item['link']['mlid'];

      // Add the item to the array.
      $menu_items[$mlid] = $item['link']['link_title'];

      if ($item['link']['has_children']) {

        // Recursively go again.
        _conditional_redirect_build_menu_links_array($item['below'], $menu_items);
      }
    }
  }
}

/**
 * Get the menu links of the current path.
 *
 * @return array|bool
 *   Returns an array of menu link ids or FALSE.
 */
function _conditional_redirect_get_menu_links_for_current_path() {

  // Get the current path.
  $path = current_path();

  // Query the menu links.
  $query = db_select('menu_links', 'm');

  // Select the menu link id.
  $query->fields('m', array('mlid'));

  // Match the link path on the current path.
  $query->condition('m.link_path', $path);

  // Execute and fetch the mlid.
  return $query->execute()->fetchCol();
}
